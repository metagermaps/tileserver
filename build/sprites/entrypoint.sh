#!/bin/bash

set -e

rm -rf ./temp/svgs
mkdir -p ./temp/svgs
cp -a ./iconset/svgs_not_included/. ./temp/svgs/
cp -a ./iconset/svgs/. ./temp/svgs/

/usr/local/bin/spreet --unique --minify-index-file --recursive ./temp/svgs ./sprite
/usr/local/bin/spreet --unique --minify-index-file --retina --recursive ./temp/svgs ./sprite@2x

rm -rf ./temp