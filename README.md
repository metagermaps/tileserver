# Tileserver

The Tileserver used by MetaGer Maps is based on [maptiler](https://github.com/maptiler/tileserver-gl). Vector tiles are generated using [planetiler](https://github.com/onthegomap/planetiler).

## Scheduled pipeline

A scheduled pipeline will take our daily updated `planet.osm.pbf` file and generate a planet.mbtiles used by out tileserver.

## Run Maputnik to edit the styles

1. `docker run -it --rm -p 8888:80 ghcr.io/maplibre/maputnik:main`
2. Open -> Import from URL -> `https://tileserver.maps.metager.de/styles/osm-dark/style.json`

## Compile SVGs of style to sprite

We are using [spreet](https://github.com/flother/spreet) to compile the map icons into a spritesheet. Currently we are using [Maki Icons](https://labs.mapbox.com/maki-icons/editor/) as a base which we will extend by our custom icons.

### Compile using Docker

1. `export SPREET_RELEASE=v0.11.0`
2. `docker build --build-arg=SPREET_RELEASE=${SPREET_RELEASE} ./build/sprites -t spreet:${SPREET_RELEASE}`
3. `docker run -it --rm -v ./styles/osm-bright:/spreet spreet:${SPREET_RELEASE} `
3. `docker run -it --rm -v ./styles/osm-dark:/spreet spreet:${SPREET_RELEASE}`